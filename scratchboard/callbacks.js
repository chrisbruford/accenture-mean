function myFunc(x,cb) {
    console.log(x);
    cb();
}

myFunc(15,callback);

function callback(){
    console.log(`I'm the callback`);
}

//function decleration
function myFunc() {
    //do things
}

//anonymous function decleration - not legal in global scope
// function() {
//     //do things
// }

//anonymous function decleration and assignment
let namedFunc = function() {
    //do things
}

console.log(myFunc);
console.log(namedFunc);

console.log(JSON.parse(`{"prop1": "stuff"}`));