const fs = require('fs');

// fs.readFile('hamlet.txt','utf-8',(err,data)=>{

//     if (err) {return console.error("woops")}

//     // console.log(data);

//     fs.writeFile('hamlet-copy.txt',data,(err)=>{
//         if (err) {return console.log(err)}
//         console.log('All done');
//     })
// });

let readStream = fs.createReadStream('hamlet.txt',{
    encoding: 'utf-8'
});

let writeStream = fs.createWriteStream('hamlet-stream-copy.txt');

let count = 0;

// readStream.on('data',function(data){
//     // console.log(data); 
//     count++;
// });

// readStream.on('end',function(){
//     console.log('All done!');
//     console.log(`I've emitted ${count} chunks`);
//     writeStream.end();
// });

// readStream.on('error',function(err){
//     console.log(err);
// })

readStream.pipe(writeStream);