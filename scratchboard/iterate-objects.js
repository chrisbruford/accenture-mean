let myObject = {
    prop1: "test",
    prop2: "iteration",
    prop3: "other prop"
}

for (prop in myObject) {
    console.log(`${prop}::${myObject[prop]}`);
}