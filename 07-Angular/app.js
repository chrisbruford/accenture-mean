// angular.module('ntcApp') //getter syntax
let app = angular.module('ntcApp',[]); //setter syntax

app.controller('firstController',function($scope){
    $scope.heading = "Wow this Angular Stuff is cool!";

    $scope.changeHeading = function() {
        $scope.heading = "Welcome to Angular!";
    }

    $scope.delegates = [
        {firstname: "Chris", surname: "Bruford", company: "QA"},
        {firstname: "Nicholas", surname: "Caughey", company: "Accenture"},
        {firstname: "Jack", surname: "Wilkinson", company: "Accenture"},
        {firstname: "Warren", surname: "Smithson", company: "Accenture"}
    ]
});