const fs = require('fs');
const events = require('events');

//CALLBACKS
// console.log('STARTING TO READ FILE');

fs.readFile('macbeth.tt','utf-8',function(err, data){
    if (err) {return console.log(err)}
    
    // console.log(data);
});

// console.log('FILE HAS BEEN READ');

//OOJS - classic
// function Car() {
//     this.wheels = 4;
//     this.accelerate = function(speed) {
//         this.speed = speed;
//     }
// }

// let myCar = new Car();

// myCar.accelerate(50);
// console.log(myCar.speed);

//OOJS - ES2015

class Vehicle {
    accelerate(speed) {
        this.speed = speed
    }
}

class Car extends Vehicle  {
    constructor() {
        super();
        this.wheels = 4;
    }
}

let myCar = new Car();

myCar.accelerate(50);
console.log(myCar.speed);


//EVENTS
let EventEmitter = events.EventEmitter;
let myEmitter = new EventEmitter();

myEmitter.on('partytime',function(data){
    console.log(data);
    console.log(`\\o/ \\o/ \\o/`);
})

myEmitter.emit('partytime',1999);
myEmitter.emit('partytime',1984);
myEmitter.emit('partytime',2000);
myEmitter.emit('partytime',1993);
myEmitter.emit('partytime',1995);

//Real events
class FileReader extends EventEmitter {
    constructor(path) {
        super();
        this.path = path;
    }

    read() {
        fs.readFile(this.path,'utf-8',(err, data)=>{
            if (err) {return console.log(err)}
            
            this.emit('read',data);
        });
    }
}

let myFileReader = new FileReader('macbeth.tt');

myFileReader.on('read',data=>console.log(data));

myFileReader.read();