function returnPromise(arg) {
    return new Promise(function(resolve,reject){
        setTimeout(function(){
            
            if (arg.constructor === Array) {
                // resolve(JSON.stringify(arg));
                // resolve(arg.toString());
                resolve(arg.join(" "));
            } else {
                reject("A reason");
            }

        },3000);
    });
}

returnPromise(["Hello","From","The","Team","At","Accenture"]).then(function(data){
    //on resolve
    console.log(data);
},function(err){
    //on reject
    console.error(err);
})