// const events = require('events');
let EventEmitter = require('events').EventEmitter;

class Bank extends EventEmitter {

    constructor() {
        super();
        this.balance = 0;
    }

    deposit(amount) {
        console.log(this.balance);
        this.balance+=amount;
        this.emit("deposit",{balance: this.balance, amount: amount})
    }
}

let bankOfBruford = new Bank();

bankOfBruford.on("deposit",function(deposit){
    console.log("Deposit made");
    console.log(`New balance: ${deposit.balance}`);
    console.log(`Deposit amount: ${deposit.amount}`);
})

bankOfBruford.deposit(1000000000);