// function doThings() {
//     return new Promise(function(resolve,reject){
//         for (let i = 0; i < 10000; i++) {
//             console.log('doing things');
//         }

//         resolve("I'm all done");
//     })
// }

// let myPromise = doThings();
// myPromise
//     .then(function(data){
//         //resolve
//         console.log(data);

//     },function(err){
//         //reject
//         console.error(err);
//     })

// console.log('done things');

console.log("Do me first");

asyncFunc()
    .then(function(data){
        console.log("Do me last");
        return "A new message"
    })
    .then(function(data){
        console.log(data);
        throw new Error("Uncaught")
    })
    .catch(function(err){
        console.log("Handled error");
    })


function asyncFunc() {
    return new Promise(function(resolve,reject){
        setTimeout(function(){
            console.log("I should be in the middle!");
            reject("Some message?");
        },2000);
    })
    
}