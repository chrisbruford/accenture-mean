const http = require('http');
const fs = require('fs');

http.createServer((req,res)=>{
    if (req.url === "/" && req.method === "GET") {
        res.writeHead(200, {'Content-Type': 'text/html'});
        
        let readStream = fs.ReadStream('public/index.html');
        readStream.pipe(res);
    } else if (req.url === "/js/app.js" && req.method === "GET") {
        res.writeHead(200, {'Content-Type': 'text/html'});
        
        let readStream = fs.ReadStream('public/js/app.js');
        readStream.pipe(res);
    } else if (req.url === "/delegates" && req.method === "GET") {
        res.writeHead(200, {'Content-Type': 'application/json'});
        res.end(`["Leanne","Aiden","Warren"]`);
    }
    
    else {
        res.writeHead(404,"You done messed up");
        res.end();
    }
}).listen(8080,function(){
    console.log("Always listening");
})