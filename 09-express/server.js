const express = require('express');
const app = express();
const delegatesRoute = require('./routes/delegates.js');
const bodyParser = require('body-parser');
const path = require('path');
const db = require('./db.js');

app.use(bodyParser.json());


//using the router

app.get('/',(req, res, next)=>{
    // let filePath = path.resolve(__dirname, './public/index.html');
    let filePath = `${__dirname}/public/index.html`;
    res.sendFile(filePath);
});

app.use(express.static(`${__dirname}/public`));

app.use('/delegates',delegatesRoute);

app.use(logRequest);

app.get('/bananas',function(req,res,next){
    res.send("I love bananas!");    
});

app.post('/bananas',function(req,res,next){
    res.send("You love bananas!");
})

// app.use(sayHello);


// function sayHello(req,res,next) {
//     res.send("Hello world");
//     next();
// }

function logRequest(req, res, next) {
    console.log("A request was made");
    next();
}

app.listen(8080,function(){
    console.log("Server is listening");
});