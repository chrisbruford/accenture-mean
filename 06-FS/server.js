const http = require('http');
const fs = require('fs');
const path = require('path');

http.createServer(function (req, res) {
    res.writeHead(200, { 'Content-Type': 'text/plain' });

    let stream = fs.ReadStream('hamlet.txt', {
        encoding: 'utf-8'
    });

    stream.pipe(res)

}).listen(8080, '127.0.0.1', function (err) {
    if (err) { return console.log(err) }
    console.log('Server on localhost:8080');
});

console.log(__dirname);
console.log(__filename);

console.log(path.resolve(__dirname,'../'));