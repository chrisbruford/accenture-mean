const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let delegateSchema = new Schema({
    firstname: {
        type: String,
        required: true
    },
    surname: String,
    dept: String
});

module.exports = mongoose.model('delegate',delegateSchema);