console.log("Scripts working");
let app = angular.module('ntcApp',[]);

app.controller('delegateController',function($scope,delegateService){

    delegateService.getDelegates().then(delegates=>{
        $scope.delegates = delegates;
    })
});

app.service('delegateService',function($http){
    this.getDelegates = function() {
        return $http.get('/delegates').then(res=>res.data);
    }
});