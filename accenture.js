const http = require('http');

let myServer = http.createServer((req,res)=>{

    if (req.url === "/") {
        res.writeHead(200,{
            'Content-Type': 'text/plain'
        });
    
        res.end("Hello world");
    } else {
        res.writeHead(404,"Are you mad?");
        res.end();
    }

    
});

myServer.listen(8080,'127.0.0.1',()=>{
    console.log('Server available');
});