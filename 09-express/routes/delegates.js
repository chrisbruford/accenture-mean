const express = require('express');
const router = express.Router();
const Delegate = require('../models/delegate.js');

router.get('/', (req, res, next) => {
    console.log('Delegates requested');
    //get delegates
    Delegate.find({},function(err,delegates){
        res.json(delegates);
    })
});

router.post('/',(req,res,next) => {
    // let firstname = req.body.firstname;
    // let surname = req.body.surname;
    // let dept = req.body.dept;
    
    let {firstname, surname, dept} = req.body;
    
    let myDelegate = {
        firstname,
        surname,
        dept
    }

    let newDelegate = new Delegate(myDelegate);

    newDelegate.save();
    
    res.send(`${firstname} added`);
});

//add route params using :param
router.get('/:id',(req,res,next)=>{
    let index = +req.params.id;
    res.json(delegates[index]);
});

module.exports = router;